<!--
SPDX-FileCopyrightText: 2019 Alan De Smet <chaos@highprogrammer.com>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

dijkstraspf: A Python implementation of Dijkstra's Shortest Path First algorithm
================================================================================

Given a graph (the type with nodes and edges), `dijkstraspf.dijkstra_spf`
returns the list of shortest paths from a given node to every other node.

See `help(dijkstraspf)` for usage.

For example, a graph like this:

```mermaid
graph LR;
 A-->|3|B;
 A-->|1|C;
 C-->|1|B;
```

    (Same graph as ASCII Art)

    A -------(3)--------- B
     \                   /
      `--(1)-- C --(1)--'

would be represented as:

    graph = [
             # src  dst length
             ( 'A', 'B', 3 ),
             ( 'A', 'C', 1 ),
             ( 'C', 'B', 1 )
            ]

The shortest paths from `A` could be calculated like so:

    paths = dijkstra_spf(graph, 'A')

One of the shortest paths from `A` to `B` could be found with:

    path = paths['B'].fullpath()

While a list of all shortest paths from `A` to `B` could be found with:

    path = paths['B'].allpaths()


TODO
----

- Render some test as an image or ASCII art and illustrate expected behavior
- More, larger tests
- Validate tests with third party SPF implementations 

